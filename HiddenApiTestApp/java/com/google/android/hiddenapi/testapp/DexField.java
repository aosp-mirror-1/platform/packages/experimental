package com.google.android.hiddenapi.testapp;

public class DexField extends DexMember {
  public DexField(String className, String name, String type) {
      super(className, name, type);
  }

  @Override
  public String toString() {
      return getJavaType() + " " + getJavaClassName() + "." + getName();
  }
}
