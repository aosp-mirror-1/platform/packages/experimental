package com.google.android.hiddenapi.testapp.tests;

import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.hiddenapi.testapp.MainActivity;

public class TestInstrumentation extends Instrumentation {

  private static final String TAG = "compat-test";

  @Override
  public void onCreate(Bundle args) {
    super.onCreate(args);
    Log.d(TAG, "MainInstrumentation.onCreate()");
    start();
  }

  @Override
  public void onStart() {
    Log.d(TAG, "MainInstrumentation.onStart()");
    Intent i = new Intent(getTargetContext(), MainActivity.class);
    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    getContext().startActivity(i);
    Log.d(TAG, "Started Activity");
  }
}
